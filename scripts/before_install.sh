#!/bin/bash

sudo apt-get update

sudo apt-get install -y \
    python \
    python-pip

sudo pip install --upgrade pip
sudo pip install flask

if [ -d /home/ubuntu/sample-app ]; then
  sudo rm -R /home/ubuntu/sample-app
  mkdir /home/ubuntu/sample-app
fi
