from __future__ import print_function
import os
import sys
from time import strftime, sleep
import boto3
from botocore.exceptions import ClientError

'''
# For testing deployment on local dev machine
VERSION_LABEL = strftime("%Y%m%d%H%M%S")
with open('./version.py', 'w') as version:
    version.write('VERSION_LABEL = "%s" ' % (VERSION_LABEL))
'''
VERSION_LABEL = str(os.getenv('BITBUCKET_BUILD_NUMBER'))
print('Uploading application version:', VERSION_LABEL)

BUCKET_KEY = os.getenv('APPLICATION_NAME') + '/' + VERSION_LABEL + \
    '-bitbucket_builds.zip'

def upload_to_s3(artifact):
    """
    Uploads an artifact to Amazon S3
    """
    try:
        client = boto3.client('s3')
    except ClientError as err:
        print("Failed to create boto3 client.\n" + str(err))
        return False
    try:
        client.put_object(
            Body=open(artifact, 'rb'),
            Bucket=os.getenv('S3_BUCKET'),
            Key=BUCKET_KEY
        )
    except ClientError as err:
        print("Failed to upload artifact to S3.\n" + str(err))
        return False
    except IOError as err:
        print("Failed to access artifact.zip in this directory.\n" + str(err))
        return False
    return True

def main():
    if not upload_to_s3('/tmp/artifact.zip'):
        sys.exit(1)

if __name__ == "__main__":
    main()
